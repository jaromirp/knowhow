package com.javagda14.knowhow.model.dto;

        import lombok.AllArgsConstructor;
        import lombok.Getter;
        import lombok.NoArgsConstructor;
        import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FilterClientsDto {
    private String email;
    private String surname;
    private String name;
}

