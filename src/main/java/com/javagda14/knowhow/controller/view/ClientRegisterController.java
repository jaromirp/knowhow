package com.javagda14.knowhow.controller.view;

import com.javagda14.knowhow.model.Client;
import com.javagda14.knowhow.model.dto.AddClientDto;
import com.javagda14.knowhow.service.ClientService;
import com.javagda14.knowhow.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Optional;

@Controller
public class ClientRegisterController {

    @Autowired
    private ClientService clientService;

    @Autowired
    private LoginService loginService;

    @GetMapping("/login")
    public String getLoginView() {
        return "client/login_form";
    }

    @GetMapping("/")
    public String getIndex(Model model) {
        Optional<Client> clientOptional = loginService.getLoggedInUser();
        if (clientOptional.isPresent()) {
            Client client = clientOptional.get();

            model.addAttribute("username", client.getEmail());
        }

        return "index";
    }

    @GetMapping("/register")
    public String getAddView(Model model) {
        // wysyłam pusty obiekt do formularza
        model.addAttribute("added_object", new AddClientDto());
        return "client/register_form";
    }

    @PostMapping("/register")
    public String getAddView(Model model, AddClientDto dto) {
        // Otrzymuję wypełniony obiekt z formularza który załadował mapping wyżej

        // tutaj dokonam walidacji i dodania user'a
        if (dto.getPassword().isEmpty() || dto.getPassword().length() < 5) {
            model.addAttribute("added_object", dto);
            model.addAttribute("error_message", "Too simple password.");
            return "client/register_form";
        }

        // udało się, rejestrujemy!
        // wewnątrz metody register powinna być walidacja nie istniejącego maila
        Optional<Client> clientOptional = clientService.register(dto);
        if (!clientOptional.isPresent()) {
            model.addAttribute("added_object", dto);
            model.addAttribute("error_message", "User with that email already exists.");
            return "client/register_form";
        }

        // przekierujemy na inną stronę
        // przekierowanie na stronę '/'
        return "redirect:/";
    }
}
