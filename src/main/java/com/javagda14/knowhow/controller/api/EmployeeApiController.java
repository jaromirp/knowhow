package com.javagda14.knowhow.controller.api;

import com.javagda14.knowhow.model.Client;
import com.javagda14.knowhow.model.dto.AddClientDto;
import com.javagda14.knowhow.model.dto.EmployeeDto;
import com.javagda14.knowhow.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping("/api/clients")
public class EmployeeApiController {

    @Autowired
    private ClientService clientService;

    @GetMapping("/")
    public ResponseEntity<List<EmployeeDto>> getEmployees() {
        //tutaj piszemy kod który ma zwrócić listę użytkowmników
        return ResponseEntity.ok(clientService.getAll()
                .stream()
                .map(employee -> new EmployeeDto(
                        employee.getName(),
                        employee.getSurname(),
                        employee.getEmail(), null))
                .collect(Collectors.toList()));

    }

    @PostMapping("/add")
    public ResponseEntity<Client> addClient(@RequestBody EmployeeDto dto) {
        clientService.register(dto);
        return ResponseEntity.ok().build();
    }
}
