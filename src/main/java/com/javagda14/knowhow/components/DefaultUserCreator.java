package com.javagda14.knowhow.components;

import com.javagda14.knowhow.model.Client;
import com.javagda14.knowhow.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class DefaultUserCreator {

    @Autowired
    public DefaultUserCreator(ClientRepository clientRepository) {
        Optional<Client> clientOptional = clientRepository.findByEmail("4dm1n@localhost");
        if (!clientOptional.isPresent()) {
            clientRepository.save(new Client(null, "4dm1n@localhost", "jar", "put", "$2a$10$ylDqLxhRb68nIT0h9bgdVOFIaZkqjxsSRuqv13J8EcEx8Bl0OocrC"));
            clientRepository.save(new Client(null, "jaromirp@wp.pl", "jaro", "putrycz", "jaromirp"));
            clientRepository.save(new Client(null, "jarojaro@wp.pl", "jarek", "putt", "jarojaro"));

        }
    }
}
