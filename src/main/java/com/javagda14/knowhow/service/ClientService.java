package com.javagda14.knowhow.service;

import com.javagda14.knowhow.model.Client;
import com.javagda14.knowhow.model.dto.AddClientDto;
import com.javagda14.knowhow.model.dto.EmployeeDto;
import com.javagda14.knowhow.model.dto.ModifyClientDto;
import com.javagda14.knowhow.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;

@Service
public class ClientService {
    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    public Optional<Client> register(AddClientDto dto) {
        // sprawdzenie czy użytkownik z podanym mailem nie istnieje.
        if (!clientRepository.findByEmail(dto.getEmail()).isPresent()) {
            Client client = new Client(/*id*/null,
                    dto.getEmail(),
                    dto.getName(),
                    dto.getSurname(),
//                    encoder.encode(dto.getPassword()),
                    dto.getPassword());

            client.setPassword(encoder.encode(client.getPassword()));

            Client newClient = clientRepository.save(client);

            return Optional.ofNullable(newClient);
        }
        return Optional.empty();
    }

    public Optional<Client> modify(ModifyClientDto dto) {
        Optional<Client> clientOptional = clientRepository.findById(dto.getIdToModify());
        if (clientOptional.isPresent()) {
            Client client = clientOptional.get();

            if (dto.getEmail() != null) {
                client.setEmail(dto.getEmail());
            }
            if (dto.getName() != null) {
                client.setName(dto.getName());
            }
            if (dto.getPassword() != null) {
                client.setPassword(dto.getPassword());
            }
            if (dto.getSurname() != null) {
                client.setSurname(dto.getSurname());
            }

            client = clientRepository.save(client);

            return Optional.of(client);
        }
        return Optional.empty();
    }

    public Optional<Client> find(Long id) {
        return clientRepository.findById(id);
    }

    public List<Client> getAll() {
        return clientRepository.findAll();
    }

    public Optional<Client> findByEmail(String email) {
        return clientRepository.findByEmail(email);
    }

    public void register(EmployeeDto dto) {
        Client client = new Client(/*id*/null,
                dto.getEmail(),
                dto.getName(),
                dto.getSurname(),
//                    encoder.encode(dto.getPassword()),
                dto.getPassword());

        client.setPassword(encoder.encode(client.getPassword()));

        Client newClient = clientRepository.save(client);

    }
}
