package com.javagda14.knowhow.repository;

import com.javagda14.knowhow.model.SubjectDescription;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubjectDescriptionRepository extends JpaRepository<SubjectDescription, Long> {
}
