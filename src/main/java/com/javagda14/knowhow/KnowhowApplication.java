package com.javagda14.knowhow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KnowhowApplication {

    public static void main(String[] args) {
        SpringApplication.run(KnowhowApplication.class, args);
    }
}
