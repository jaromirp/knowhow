package com.javagda14.knowhow.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ModifyClientDto {
    private Long idToModify;
    @Email
    private String email;
    private String name;
    private String surname;
    private String password;
}

