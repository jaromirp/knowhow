package com.javagda14.knowhow.controller.view;

import com.javagda14.knowhow.model.SubjectDescription;
import com.javagda14.knowhow.service.LoginService;
import com.javagda14.knowhow.service.SubjectDescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.security.auth.Subject;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/view/subjectdescription/")
public class SubjectDescriptionContoller {

    @Autowired
    private SubjectDescriptionService subjectDescriptionService;

    @Autowired
    private LoginService loginService;

    @GetMapping("/list")
    public String getLoginView(Model model) {
        List<SubjectDescription> list = subjectDescriptionService.findAll();
        model.addAttribute("subject_list", list);

        return "client/subject_list";
    }

    @GetMapping("/add")
    public String addForm(Model model) {
        SubjectDescription newSubjectDescription = new SubjectDescription();
        model.addAttribute("new_subject", newSubjectDescription);

        return "client/subject_form";
    }

    @PostMapping("/add")
    public String addForm(Model model, SubjectDescription dto) {
        // todo: dodać 'dto' do bazy
        // todo*: przed dodaniem do bazy, ustaw obiektowi ownera jako obecnie zalogowanego użytkownika
        // walidacja czy pole subject i description jest wypełnione i nie jest puste
        if (dto.getSubject().isEmpty() || dto.getDescription().isEmpty() || dto.getDescription().length()>5000 ) {
            //model.addAttribute("added_object", dto.getId());
            model.addAttribute("new_subject", dto); //("added_object", dto);
            model.addAttribute("error_message", "Empty Subject or Description or Description > 5000");

            return "client/subject_form";
        }

        Optional<SubjectDescription> subjectDescriptionOptional = subjectDescriptionService.register(dto);

        return "redirect:/view/subjectdescription/list";
    }

    @GetMapping("/modify/{id}")
    public String modifyForm(Model model, @PathVariable(name = "id") Long id) {
        // todo: skorzystaj z serwisu i pobierz z niego subjectDescription o identyfikatorze ze zmiennej 'id'
        Optional<SubjectDescription> subjectDescription = subjectDescriptionService.findById(id);

        // jeśli odnjadziesz taki subject to:
        // -> dodaj obiekt do modelu (zmienna 'model')
        // -> załaduj widok
        if (subjectDescription.isPresent()) {
            model.addAttribute("new_subject", subjectDescription.get());
            model.addAttribute("error_message", "Empty Subject or empty Description or both");

            return "client/subject_form";
        } else {
            //model.addAttribute("error_message", "Empty Subject or empty Description or both");
        }
        return "redirect:/view/subjectdescription/list";
    }
}


