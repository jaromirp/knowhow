package com.javagda14.knowhow.service;

import com.javagda14.knowhow.model.SubjectDescription;
import com.javagda14.knowhow.repository.SubjectDescriptionRepository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import com.javagda14.knowhow.repository.ClientRepository;

import java.util.*;

@Service
public class SubjectDescriptionService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private SubjectDescriptionRepository subjectDescriptionRepository;

    public List<SubjectDescription> findAll() {
        return subjectDescriptionRepository.findAll();
    }

    public Optional<SubjectDescription> register(SubjectDescription dto) { //dto
        SubjectDescription subjectDescription1 = new SubjectDescription();
        subjectDescription1.setId(dto.getId());
        subjectDescription1.setSubject(dto.getSubject());
        subjectDescription1.setDescription(dto.getDescription());
        subjectDescription1.setOwner(dto.getOwner());

        subjectDescription1 = subjectDescriptionRepository.save(subjectDescription1);

        return Optional.of(subjectDescription1);
    }

    public Optional<SubjectDescription> findById(Long id) {
        return subjectDescriptionRepository.findById(id);
    }
}
