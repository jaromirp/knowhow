//package com.javagda14.knowhow.model;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import javax.persistence.*;
//import java.time.LocalDateTime;
//
//@Entity
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//public class Device {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Long id;
//    private String name;  //name
//    private String value;  //value + information
//    private LocalDateTime dateAdded;
//
//    @ManyToOne
//    @JsonIgnore
//    private Client owner;
//}
